package quiz;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Answer {
	public String playerInput() {
		BufferedReader playerAnswer = new BufferedReader(new InputStreamReader(System.in));
		String answer = "";
		System.out.print("Antwort: ");
		try {
			answer = playerAnswer.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return answer;
	}
	
	public String auswahl() {
		BufferedReader menue = new BufferedReader(new InputStreamReader(System.in));
		String auswahl="";
		try {
			auswahl=menue.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return auswahl;
	}
}
