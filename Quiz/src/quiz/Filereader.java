package quiz;

import java.io.*;
import java.util.Scanner;

public class Filereader {
	private String text;
	BufferedReader bf;

	public boolean chooseList() {
		int topic = 0;
		boolean choose;
			System.out.print(
					"Bitte aus folgenden Themen w�hlen.\nGeographie	(1)\nWissenschaft	(2)\nGeschichte	(3)\nLiteratur	(4)\nThema:\t");
			try {
				bf = new BufferedReader(new InputStreamReader(System.in));
				topic =Integer.parseInt(bf.readLine()); 
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			switch (topic) {
			case 1:
				choose = true;
				File questionGeo = new File("docs/Geographie");
				try {
					bf = new BufferedReader(new FileReader(questionGeo));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				break;
			case 2:
				choose = true;
				File questionSci = new File("docs/Wissenschaft");
				try {
					bf = new BufferedReader(new FileReader(questionSci));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				break;
			case 3:
				choose = true;
				File questionHist = new File("docs/Geschichte");
				try {
					bf = new BufferedReader(new FileReader(questionHist));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				break;
			case 4:
				choose = true;
				File questionLit = new File("docs/Literatur");
				try {
					bf = new BufferedReader(new FileReader(questionLit));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				break;
			default:
				choose=false;
				System.out.println("Um ein Themengebiet zu w�hlen, bitte die Zahlen 1 - 4 verwenden");
				break;
			}
			return choose;
		}

	public void nextLine() {
		try {
			text = bf.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getText() {
		return text;
	}
}