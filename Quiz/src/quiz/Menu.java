package quiz;

public class Menu {
	Answer player = new Answer();
	Quiz game = new Quiz();

	public String firstMenu() {
		String auswahl;
		System.out.println("Willkommen zum Wissensquiz!");
		System.out.print("Neues Spiel?: ");
		return auswahl = player.auswahl();
	}

	public void repeat() {
		String auswahl;
		System.out.print("Neue Runde?: ");
		auswahl = player.auswahl();
			if(auswahl.equalsIgnoreCase("ja")) {
				game.run(auswahl);
		}else {
			System.out.println("Bis zum n�chsten mal");
		}
	}
	
	public void menuHelp() {
	System.out.println("------ Spieleanleitung ------");
	System.out.println("Um die Anleitung aufzurufen kannst du jederzeit ?/hilfe/anleitung eingeben.");
	System.out.println("Ziel des Spieles ist es m�glichst viele Fragen richtig zu beantworten");
	System.out.println("Die Themenwahl erfolgt �ber die Zahlen 1-4, und die Antwortauswahl erfolgt �ber die buchstaben A-D");
	System.out.println("Viel Spa�");
	System.out.println("-----------------------------");
	}
}
