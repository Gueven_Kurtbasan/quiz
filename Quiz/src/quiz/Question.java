package quiz;

public class Question {

	public String[] builder(Filereader questionList) {
		String[] questionLine = questionList.getText().split(";");
		System.out.println("Frage: " + questionLine[0]);
		System.out.println("A: " + questionLine[1]);
		System.out.println("B: " + questionLine[2]);
		System.out.println("C: " + questionLine[3]);
		System.out.println("D: " + questionLine[4]);
		return questionLine;
	}
}
