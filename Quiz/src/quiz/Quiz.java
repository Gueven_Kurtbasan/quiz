package quiz;

public class Quiz {
	public void run(String auswahl) {
		Answer player = new Answer();
		Filereader questionList = new Filereader();
		Question screenOutput = new Question();
		Menu menu = new Menu();
		boolean list;
		String answer;
		int points = 0;
		String[] questionLine;

		if (auswahl.equalsIgnoreCase("ja")) {
			do {
				list = questionList.chooseList();
			} while (list == false);
			// questionList.nextLine() liest �ber einen BufferedReader Zeilenweise aus einem
			// .txt Dokument, dann wird gepr�ft mit questionList gepr�ft ob eine weitere
			// Zeile vorhanden ist,
			// wenn nicht ist das Quiz zu ende, anschlie�end wird die n�chste Zeile
			// eingelesen
			for (questionList.nextLine(); questionList.getText() != null; questionList.nextLine()) {
				// questionList.getText().split(";"); nimmt die Zeile und trennt sie anhand des
				// Semikolons auf um sie dann anschliessend in einem StringArray �ber den Index
				// zuzuweisen
				//screenOutput.builder(questionList) bekommt die Frage �bergeben und gibt sie in der Console aus
				questionLine=screenOutput.builder(questionList);
				// Die boolean Variable legt �ber correctAns Fest ob die Antwort korrekt ist, so
				// kann in einer while-Schleife diese Bedingung gepr�ft werden
				boolean correctAns = false;
				while (correctAns == false) {
					answer = player.playerInput();
					if (answer.equalsIgnoreCase("hilfe") || answer.equalsIgnoreCase("anleitung") || answer.equals("?")) {
						menu.menuHelp();
					}
					// Das immer die 6. Stelle des questionLine Arrays verwendet wird,
					// liegt am Format der .txt Datei - > "Fragestellung (index:0) ; Antwort
					// A(index:1); Antwort B(2); Antwort C(3); Antwort D(4); L�sung(In diesem Fall
					// der ensprechende Buchstabe)(index:6);
					else if (answer.toUpperCase().equals(questionLine[5])) {
						System.out.println("Richtig!");
						points = points + 10;
						correctAns = true;
					} else if (answer.toUpperCase().equals("A") || answer.toUpperCase().equals("B")
							|| answer.toUpperCase().equals("C") || answer.toUpperCase().equals("D")) {
						System.out.println("Falsche Antwort! Korrekt w�re " + questionLine[5] + " gewesen");
						correctAns = true;
					} else {
						System.out.println("Bitte w�hlen zwischen 'A' 'B' 'C' oder 'D'");
					}
				}
			}
			System.out.print("Deine Endpunktzahl betr�gt: " + points + " punkte \n");
			menu.repeat();
		} else if (auswahl.equalsIgnoreCase("hilfe") || auswahl.equalsIgnoreCase("anleitung") || auswahl.equals("?")) {
			menu.menuHelp();
			auswahl = menu.firstMenu();
			run(auswahl);
		} else {
			System.out.println("Bis zum n�chsten mal");
		}
	}
}
