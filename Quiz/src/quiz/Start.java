package quiz;

public class Start {

	public static void main(String[] args) {
		Quiz game = new Quiz();
		Menu menu =new Menu();
		
		menu.menuHelp();
		String auswahl = menu.firstMenu();
		game.run(auswahl);
	}

}
